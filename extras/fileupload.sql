use starter_login;

DROP TABLE IF EXISTS filedata;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS downloads;

CREATE TABLE users
(
	username varchar(255) NOT NULL,
	password varchar(255) NOT NULL,
	PRIMARY KEY (username)
);

INSERT INTO users(username, password)
	values
	(
		'test',
		'testpw'
	);

CREATE TABLE filedata 
(
	FileId int NOT NULL AUTO_INCREMENT,
	FileName varchar(255),
	FilePath varchar(1023),
	FileVersion varchar(255),
	FileDesc varchar(1023),
	FileMD5 varchar(255), -- MD5 is 32 digit HEX
	DownloadCount varchar(255),
	PRIMARY KEY (FileId) -- MD5 later? ; Download count?(also as sorting key)
);

-- sample data delete later

insert into filedata (FileName, FilePath, FileVersion, FileDesc)
	values 
	(
		'eclipse',
		'C:/eclipse',
		'4.3',
		'what an awesome software'
	),
	(
		'intellij',
		'C:/intellij',
		'13',
		'what an awesome software, better than eclipse'
	);
		
insert into filedata (FileName)
values
( 'notepad++'),('sublimetext'),('xampp'),('cmd'),('sts'),('postgres'),('vlc'),('virtualbox'),('chrome'),('pidgin');

CREATE TABLE downloads
(
	DownloadId int NOT NULL AUTO_INCREMENT,
	FileName
	
)