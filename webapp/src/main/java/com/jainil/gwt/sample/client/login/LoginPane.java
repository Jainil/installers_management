package com.jainil.gwt.sample.client.login;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.github.gwtbootstrap.client.ui.HelpBlock;
import com.github.gwtbootstrap.client.ui.Modal;
import com.github.gwtbootstrap.client.ui.PasswordTextBox;
import com.github.gwtbootstrap.client.ui.TextBox;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.jainil.gwt.sample.client.welcome.WelcomePage;
import com.jainil.gwt.sample.domain.dto.User;
import com.jainil.gwt.sample.domain.login.LoginService;
import com.jainil.gwt.sample.domain.login.LoginServiceAsync;

public class LoginPane extends PopupPanel {

    private static final int ONE_HOUR = 1000 * 3600;
    private static Logger logger = Logger.getLogger("GWTLoginLogger");
    private static LoginPaneUiBinder uiBinder = GWT.create(LoginPaneUiBinder.class);
    private final LoginServiceAsync loginService = GWT.create(LoginService.class);

    interface LoginPaneUiBinder extends UiBinder<Widget, LoginPane> {
    }

    // CHECKSTYLE:OFF
    @UiField
    Modal m;
    @UiField
    TextBox userName;
    @UiField
    PasswordTextBox passWord;
    @UiField
    HelpBlock errMessageBlock;

    // CHECKSTYLE:ON

    /**
     * 
     */
    public LoginPane() {
        add(uiBinder.createAndBindUi(this));
    }

    /**
     * 
     */
    public void showModal() {
        m.show();
    }

    @UiHandler("signupButton")
    void onSignUp(ClickEvent e) {
        try {
            loginService.signUp(userName.getText().trim(), passWord.getText().trim(),
                    new AsyncCallback<User>() {

                        @Override
                        public void onFailure(Throwable e0) {
                            logger.log(Level.INFO, "Sign Up Failure " + e0.getMessage());
                            errMessageBlock.setText("Auth Failure: " + e0.getMessage());
                        }

                        @Override
                        public void onSuccess(User user) {
                            logger.log(Level.INFO, "New sign up: " + user.getUsername());
                            m.hide();
                            RootPanel.get().add(new WelcomePage());
                        }
                    });
        } catch (Exception e1) {
            logger.log(Level.INFO, "Sign Up Failure (2) " + e1.getMessage());
            errMessageBlock.setText("Sign Up service unavilabe");
        }
    }

    @UiHandler("loginButton")
    void onClick(ClickEvent e) {

        String user;
        String pwd;
        try {
            user = validatedUsername(userName.getText().trim());
            pwd = validatedPassword(passWord.getText().trim());
        } catch (Exception e2) {
            errMessageBlock.setText("Please enter valid username and password");
            return;
        }

        try {
            loginService.getLogin(user, pwd, new AsyncCallback<User>() {

                @Override
                public void onFailure(Throwable e0) {
                    logger.log(Level.INFO, "Auth Failure " + e0.getMessage());
                    errMessageBlock.setText("Sorry wrong username or password");
                }

                @Override
                public void onSuccess(User user) {
                    logger.log(Level.INFO, "Success: " + user.getUsername());
                    // Expires in one hour
                    Date expires = new Date(System.currentTimeMillis() + ONE_HOUR);
                    Cookies.setCookie("user", user.getUsername(), expires);
                    m.hide();
                    RootPanel.get().add(new WelcomePage());
                }
            });
        } catch (Exception e1) {
            logger.log(Level.INFO, "Auth Failure (2) " + e1.getMessage());
            errMessageBlock.setText("Sorry auth service unavilabe");
        }
    }

    private String validatedPassword(String aTrim) throws Exception {
        if (aTrim.matches("^[a-zA-Z0-9]{6,20}$")) {
            return aTrim;
        } else {
            throw new Exception(
                    "Invalid Password String (Must be alphanumeric and atleast 6 characters)");
        }
    }

    private String validatedUsername(String aTrim) throws Exception {
        if (aTrim.matches("^[a-zA-Z0-9]{1,20}+$")) {
            return aTrim;
        } else {
            throw new Exception("Invalid Username (Must be alphanumeric)");
        }
    }

}
