package com.jainil.gwt.sample.client.list;

import java.util.List;
import java.util.logging.Logger;

import com.github.gwtbootstrap.client.ui.CellTable;
import com.github.gwtbootstrap.client.ui.SimplePager;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.jainil.gwt.sample.domain.dto.FileMetaData;
import com.jainil.gwt.sample.domain.listing.ListingService;
import com.jainil.gwt.sample.domain.listing.ListingServiceAsync;

public class FileListing extends Composite {

    // private static final int HTTP_OK_CODE = 200;
    private static final int PAGE_SIZE = 10;
    private static Logger logger = Logger.getLogger("GWTListingLogger");
    // private static final String JSON_URL = GWT.getHostPageBaseURL() +
    // "listing";

    private final CellTable<FileMetaData> table = new CellTable<FileMetaData>();
    private final SimplePager pager = new SimplePager();
    private final VerticalPanel panel = new VerticalPanel();

    private final ListingServiceAsync listingService = GWT.create(ListingService.class);

    private AsyncDataProvider<FileMetaData> provider;

    private int tableSize;

    /**
     * 
     */
    public FileListing() {
        setTableSize();
        // move setup table to setTableSize onSuccess call - refresh table
        // onsuccess
        // setupTable();
        pager.setDisplay(table);
        panel.add(pager);
        panel.add(table);
        initWidget(panel);
    }

    public int getTableSize() {
        return this.tableSize;
    }

    public void setTableSize(int aTableSize) {
        this.tableSize = aTableSize;
    }

    /**
     * 
     */
    public void setupTable() {

        table.setPageSize(PAGE_SIZE);

        TextColumn<FileMetaData> nameColumn = new TextColumn<FileMetaData>() {
            @Override
            public String getValue(FileMetaData object) {
                return object.getFileName();
            }
        };
        table.addColumn(nameColumn, "File Name");

        TextColumn<FileMetaData> versionColumn = new TextColumn<FileMetaData>() {
            @Override
            public String getValue(FileMetaData object) {
                return object.getFileVersion();
            }
        };
        table.addColumn(versionColumn, "Version");

        TextColumn<FileMetaData> pathColumn = new TextColumn<FileMetaData>() {
            @Override
            public String getValue(FileMetaData object) {
                return object.getFilePath();
            }
        };
        table.addColumn(pathColumn, "Path");

        TextColumn<FileMetaData> descColumn = new TextColumn<FileMetaData>() {
            @Override
            public String getValue(FileMetaData object) {
                return object.getFileDesc();
            }
        };
        table.addColumn(descColumn, "Description");

        provider = new AsyncDataProvider<FileMetaData>() {

            @Override
            protected void onRangeChanged(HasData<FileMetaData> display) {
                final int start = display.getVisibleRange().getStart();
                int end = start + display.getVisibleRange().getLength();
                end = end >= tableSize ? tableSize : end;
                listingService.getFileList(start, end, new AsyncCallback<List<FileMetaData>>() {

                    @Override
                    public void onSuccess(List<FileMetaData> outList) {
                        table.setRowData(start, outList);
                        logger.info("first row: " + outList.get(0).getFileName());
                    }

                    @Override
                    public void onFailure(Throwable aCaught) {
                        // TODO Auto-generated method stub

                    }
                });
            }
        };

        provider.addDataDisplay(table);
        provider.updateRowCount(tableSize, true);

    }

    /**
     * @param start
     * @param end
     * @return
     */
    // protected List<FileMetaData> getList(final int start, int end) {
    //
    // final List<FileMetaData> outList = new LinkedList<FileMetaData>();
    //
    // StringBuilder params = new StringBuilder("?start=");
    // params.append(start);
    // params.append("&end=");
    // params.append(end);
    //
    // RequestCallback getlistCallback = new RequestCallback() {
    //
    // @Override
    // public void onResponseReceived(Request arg0, Response jsonFileList) {
    // if (jsonFileList.getStatusCode() != HTTP_OK_CODE) {
    // logger.info("Resource N/A");
    // } else {
    // logger.info("Successfully retrieved the JSON listing: "
    // + jsonFileList.getText());
    // JsArray<FileMetaData> fileJsArray =
    // JsonUtils.safeEval(jsonFileList.getText());
    // for (int i = 0; i < fileJsArray.length(); i++) {
    // outList.add(fileJsArray.get(i));
    // logger.info("Got file: " + fileJsArray.get(i).getFileName());
    // }
    // table.setRowData(start, outList);
    // }
    // }
    //
    // @Override
    // public void onError(Request request, Throwable exception) {
    // logger.info("Failed to retrive listing");
    // }
    // };
    //
    // try {
    // logger.info("trying to fetch listing at " + JSON_URL + params);
    // makeRequest(JSON_URL + params, getlistCallback);
    // return outList;
    // } catch (RequestException e) {
    // e.printStackTrace();
    // logger.info(e.getMessage());
    // logger.info("Can't retrieve Listing");
    // return null;
    // }
    // }

    /**
     * 
     */
    protected void setTableSize() {

        // String url = JSON_URL + "?size";
        // RequestCallback setSizeCallback = new RequestCallback() {
        //
        // @Override
        // public void onResponseReceived(Request request, Response response) {
        // if (response.getStatusCode() == HTTP_OK_CODE) {
        // tableSize = Integer.valueOf(response.getText());
        // logger.info("Table size set to: " + tableSize);
        // setupTable();
        // }
        // }
        //
        // @Override
        // public void onError(Request request, Throwable exception) {
        // // TODO Auto-generated method stub
        //
        // }
        // };
        // try {
        // makeRequest(url, setSizeCallback);
        // } catch (RequestException e) { // TODO Auto-generated catch block
        // e.printStackTrace();
        // }

        listingService.getFileCount(new AsyncCallback<Integer>() {

            @Override
            public void onFailure(Throwable aCaught) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onSuccess(Integer fileCount) {
                setTableSize(fileCount);
                setupTable();
            }
        });

    }

    // private void makeRequest(String url, RequestCallback callback) throws
    // RequestException {
    // RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, url);
    // // builder.setHeader("Content-type",
    // // "application/x-www-form-urlencoded"); if making a post request
    // builder.sendRequest(null, callback);
    // }
    //
    /**
     *
     */
    public void refreshTable() {
        // setTableSize();
    }
}