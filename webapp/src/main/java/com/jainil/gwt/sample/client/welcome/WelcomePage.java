package com.jainil.gwt.sample.client.welcome;

import java.util.logging.Logger;

import com.github.gwtbootstrap.client.ui.ProgressBar;
import com.github.gwtbootstrap.client.ui.TextBox;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitEvent;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.jainil.gwt.sample.client.list.FileListing;
import com.jainil.gwt.sample.client.login.LoginPane;

// TODO fix & put this on top
/*
 * @UiField Button button;
 * 
 * public WelcomePage(String firstName) {
 * initWidget(uiBinder.createAndBindUi(this)); button.setText(firstName); }
 * 
 * @UiHandler("button") void onClick(ClickEvent e) {
 * 
 * }
 * 
 * public void setText(String text) { button.setText(text); }
 * 
 * public String getText() { return button.getText(); }
 */

public class WelcomePage extends Composite {

    private static final String FILE_UPLOAD_SERVLET = "file-upload";
    private static final int PERCENT = 100;
    private static final String FILE_UPLOAD_URL = GWT.getHostPageBaseURL() + FILE_UPLOAD_SERVLET;
    private static Logger log = Logger.getLogger("GwtWelcomeLogger");

    private static WelcomePageUiBinder uiBinder = GWT.create(WelcomePageUiBinder.class);

    // private final LoginPane loginPane = new LoginPane();
    private final FileListing fileListing = new FileListing();

    // CHECKSTYLE:OFF
    @UiField
    FlowPanel fileListingPanel;
    @UiField(provided = true)
    FormPanel main_form;
    @UiField(provided = true)
    FileUpload upload_file;
    @UiField
    FlowPanel formPanel;
    @UiField
    TextBox upload_fileName;
    ProgressBar bar;

    // CHECKSTYLE:ON

    interface WelcomePageUiBinder extends UiBinder<Widget, WelcomePage> {
    }

    /**
     * 
     */
    public WelcomePage() {
        setupForm();
        initWidget(uiBinder.createAndBindUi(this));
        fileListingPanel.add(fileListing);
    }

    @UiHandler("logout")
    void onLogout(ClickEvent e) {
        // TODO implement logout function
        RootPanel.get().clear();
        LoginPane pane = new LoginPane();
        RootPanel.get().add(pane, 0, 0);
        pane.showModal();
    }

    @UiHandler("upload_button")
    void submit(ClickEvent e) {
        main_form.submit();
    }

    @UiHandler("main_form")
    void onSubmit(SubmitEvent e) {
        if (upload_fileName.getText().trim().length() == 0) {
            Window.alert("Text box should not be empty");
            e.cancel();
        } else {
            checkProgress();
            // TODO load progress bar with zero progress
            bar = new ProgressBar();
            formPanel.add(bar);
        }
    }

    @UiHandler("main_form")
    void onSubmitComplete(SubmitCompleteEvent e) {
        Window.alert("Upload finished!");
        // TODO update listing
        fileListing.refreshTable();
        // Hide progress bar
        formPanel.remove(bar);
    }

    private void checkProgress() {
        RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, FILE_UPLOAD_URL);

        try {
            builder.sendRequest(null, new RequestCallback() {

                @Override
                public void onResponseReceived(Request request, Response response) {
                    Double progress = Double.parseDouble(response.getText());
                    log.info(response.getText());
                    // Update progress bar
                    bar.setPercent((int) (progress * PERCENT));
                    if (progress != 1.0) {
                        checkProgress();
                    }
                }

                @Override
                public void onError(Request request, Throwable exception) {
                    // TODO Auto-generated method stub

                }
            });
        } catch (RequestException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void setupForm() {
        main_form = new FormPanel();
        main_form.setAction(FILE_UPLOAD_SERVLET);
        main_form.setEncoding(FormPanel.ENCODING_MULTIPART);
        main_form.setMethod(FormPanel.METHOD_POST);

        upload_file = new FileUpload();
        upload_file.setName("upload-file");
    }

}