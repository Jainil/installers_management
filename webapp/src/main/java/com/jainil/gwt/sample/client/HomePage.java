package com.jainil.gwt.sample.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;
import com.jainil.gwt.sample.client.login.LoginPane;

public class HomePage implements EntryPoint {

    /**
     * 
     */
    public HomePage() {
    }

    @Override
    public void onModuleLoad() {
        LoginPane pane = new LoginPane();
        RootPanel.get().add(pane, 0, 0);
        pane.showModal();
    }
}
