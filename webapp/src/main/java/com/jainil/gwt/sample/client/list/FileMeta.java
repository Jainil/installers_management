package com.jainil.gwt.sample.client.list;

import com.google.gwt.core.client.JavaScriptObject;

public class FileMeta extends JavaScriptObject {

    /**
     * 
     */
    protected FileMeta() {
    }

    /**
     * @return
     */
    public final native String getFilename()
    /*-{
		return this.filename
    }-*/;

    /**
     * @return
     */
    public final native String getDescription()
    /*-{
		return this.description
    }-*/;

    /**
     * @return
     */
    public final native String getVersion()
    /*-{
		return this.version
    }-*/;

    /**
     * @return
     */
    public final native String getPath()
    /*-{
		return this.path
    }-*/;

}