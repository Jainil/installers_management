package com.jainil.gwt.sample.domain;

import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.jainil.gwt.sample.domain.dto.FileMetaData;
import com.jainil.gwt.sample.domain.listing.ListingService;

public class ListingTest {

    // Size of filedata table after the sql init script
    // private fina`l Integer expectedSize = new Integer(12);

    /**
     * 
     */
    @Test
    public void testListSize() {
        try {
            @SuppressWarnings("resource")
            ApplicationContext context = new ClassPathXmlApplicationContext(
                    "./META-INF/spring/applicationContext-login.xml");
            ListingService service = (ListingService) context.getBean("listingService");
            List<FileMetaData> fileList = service.getFileList(10, 20);
            for (FileMetaData file : fileList) {
                System.out.println(file.getFileName());
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            fail();
        }
    }
}
