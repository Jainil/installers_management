package com.jainil.gwt.sample.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.junit.Test;

import com.jainil.gwt.sample.domain.dto.FileMetaData;

public class SerializationTester {

    /**
     * 
     */
    @Test
    public void test() {
        try {
            FileMetaData data = new FileMetaData();
            new ObjectOutputStream(new ByteArrayOutputStream()).writeObject(data);

            // construct test object
            FileMetaData original = new FileMetaData();
            original.setFileName("String Value");

            // serialize
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(out);
            oos.writeObject(original);
            oos.close();

            // deserialize
            byte[] pickled = out.toByteArray();
            InputStream in = new ByteArrayInputStream(pickled);
            ObjectInputStream ois = new ObjectInputStream(in);
            Object o = ois.readObject();
            FileMetaData copy = (FileMetaData) o;

            // test the result
            assertEquals("String Value", copy.getFileName());
            assertEquals("", "");
        } catch (Exception e) {
            fail();
        }
    }
}
