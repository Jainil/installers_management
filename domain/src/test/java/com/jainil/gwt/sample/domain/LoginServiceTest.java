package com.jainil.gwt.sample.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import com.jainil.gwt.sample.domain.login.LoginServiceImpl;

public class LoginServiceTest {

    /**
     * 
     */
    @Test
    public void test() {
        LoginServiceImpl service = new LoginServiceImpl();
        try {
            String username = "test";
            String password = "testpw";
            assertEquals("Returned username is not as expected", username,
                    service.getLogin(username, password).getUsername());
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getMessage());
            fail("Exception Occured: " + e.getMessage());
        }
    }
}