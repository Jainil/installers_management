package com.jainil.gwt.sample.domain;

import static org.junit.Assert.fail;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.jainil.gwt.sample.domain.db.PersistenceAccess;

public class SqlAccessTest {

    /**
     * Test passes if connection established without exception
     */
    @Test
    public void test() {
        try {
            @SuppressWarnings("resource")
            ApplicationContext context = new ClassPathXmlApplicationContext(
                    "./META-INF/spring/applicationContext-login.xml");
            PersistenceAccess access = (PersistenceAccess) context.getBean("dbAccess");
            String[] args = { "jainil" };
            List<Map<String, String>> res = access.executeQuery(
                    "select * from users where username= ?", args);
            System.out.println(res.get(0).values());
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getMessage());
            fail("Exception occured");
        }
    }
}
