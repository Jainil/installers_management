package com.jainil.gwt.sample.domain.db;

import java.util.List;
import java.util.Map;

public interface PersistenceAccess {

    /**
     * @throws PersistenceException
     */
    void close();

    /**
     * @param query
     * @return
     * @throws PersistenceException
     */
    List<Map<String, String>> executeQuery(String SQL, String[] args) throws PersistenceException;

    /**
     * @param query
     * @throws PersistenceException
     */
    void update(String query) throws PersistenceException;

}
