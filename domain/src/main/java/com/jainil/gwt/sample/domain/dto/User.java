package com.jainil.gwt.sample.domain.dto;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class User implements IsSerializable, Serializable {

    private static final long serialVersionUID = 8369879005400366187L;
    private String username;

    /**
     * 
     */
    public User() {
    }

    /**
     * @param userName
     */
    public User(String userName) {
        this.username = userName;
    }

    /**
     * @return
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * @param userName
     */
    public void setUsername(String userName) {
        this.username = userName;
    }

}
