package com.jainil.gwt.sample.domain.listing;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.jainil.gwt.sample.domain.dto.FileMetaData;

public interface ListingServiceAsync {

    /**
     * @param start
     * @param end
     * @param callback
     */
    void getFileList(int start, int end, AsyncCallback<List<FileMetaData>> callback);

    /**
     * @param callback
     */
    void getFileCount(AsyncCallback<Integer> callback);
}
