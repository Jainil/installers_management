package com.jainil.gwt.sample.domain.listing;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.jainil.gwt.sample.domain.dto.FileMetaData;

/**
 * Servlet implementation class DirectoryListingServlet
 */
public class DirectoryListingServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    // private static Logger logger = Logger.getLogger("ServletListingLogger");

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response) Send file list from index 1 to max in request
     */
    @SuppressWarnings("unchecked")
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        DirectoryListingDAOImpl dataDaoImpl = new DirectoryListingDAOImpl();
        PrintWriter writer = response.getWriter();

        if (request.getParameter("size") != null) {
            try {
                writer.write(String.valueOf(dataDaoImpl.getListSize()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            writer.flush();
            writer.close();
            return;
        }

        Collection<FileMetaData> fileList = null;
        try {
            Integer start = Integer.decode(request.getParameter("start"));
            Integer end = Integer.decode(request.getParameter("end"));
            fileList = dataDaoImpl.getList(start, end);
        } catch (Exception e) {
            e.printStackTrace();
        }

        JSONArray fileJsonArray = new JSONArray();

        if (fileList != null) {
            for (FileMetaData file : fileList) {
                JSONObject object = new JSONObject();
                object.putAll(file.getDataMap());
                fileJsonArray.add(object);
            }
        }
        writer.print(fileJsonArray.toJSONString());
        writer.flush();
        writer.close();
    }
}
