package com.jainil.gwt.sample.domain.login;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.jainil.gwt.sample.domain.dto.User;

public class LoginServiceImpl extends RemoteServiceServlet implements LoginService {

    private static final long serialVersionUID = -6707814390068091499L;
    private LoginServiceDAO dao;

    public void setDao(LoginServiceDAO aDao) {
        this.dao = aDao;
    }

    @Override
    public User getLogin(String username, String password) throws AuthServiceException {

        try {
            return this.dao.fetch(username, password);
            // HttpSession session =
        } catch (AuthServiceException ae) {
            throw ae;
        } catch (Exception e) {
            throw new AuthServiceException(e.getMessage());
        }
    }

    @Override
    public User signUp(String username, String password) throws AuthServiceException {
        try {
            this.dao.put(username, password);
            return new User(username);
        } catch (Exception e) {
            throw new AuthServiceException(e.getMessage());
        }
    }
}