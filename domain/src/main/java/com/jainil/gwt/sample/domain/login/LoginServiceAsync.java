package com.jainil.gwt.sample.domain.login;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.jainil.gwt.sample.domain.dto.User;

public interface LoginServiceAsync {

    /**
     * @param username
     * @param password
     * @param callback
     */
    void getLogin(String username, String password, AsyncCallback<User> callback);

    /**
     * @param username
     * @param password
     * @param callback
     * @return
     */
    void signUp(String username, String password, AsyncCallback<User> callback);
}
