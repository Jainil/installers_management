package com.jainil.gwt.sample.domain.listing;

import java.util.List;

import com.jainil.gwt.sample.domain.dto.FileMetaData;

public class ListingServiceImpl implements ListingService {

    private DirectoryLisitngDAO dao;

    public void setDao(DirectoryLisitngDAO aDao) {
        this.dao = aDao;
    }

    @SuppressWarnings("boxing")
    @Override
    public List<FileMetaData> getFileList(int aStart, int aEnd) throws Exception {
        return this.dao.getList(aStart, aEnd);
    }

    @Override
    public Integer getFileCount() throws Exception {
        return this.dao.getListSize();
    }

}
