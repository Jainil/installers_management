package com.jainil.gwt.sample.domain.listing;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.jainil.gwt.sample.domain.dto.FileMetaData;

@RemoteServiceRelativePath("listing.rpc")
public interface ListingService extends RemoteService {

    /**
     * @param start
     * @param end
     * @return
     * @throws Exception
     */
    List<FileMetaData> getFileList(int start, int end) throws Exception;

    /**
     * @return
     * @throws Exception
     */
    Integer getFileCount() throws Exception;
}
