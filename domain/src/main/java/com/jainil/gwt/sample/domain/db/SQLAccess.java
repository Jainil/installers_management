package com.jainil.gwt.sample.domain.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class SQLAccess implements PersistenceAccess {

    // private static Logger log = Logger.getLogger(SQLAccess.class.getName());

    private ComboPooledDataSource cpds;

    public void setCpds(ComboPooledDataSource aCpds) {
        this.cpds = aCpds;
    }

    /**
     * @param query
     * @return
     * @throws SQLException
     */
    @Override
    public List<Map<String, String>> executeQuery(String SQL, String[] args)
            throws PersistenceException {
        try {
            Connection conn = this.cpds.getConnection();
            PreparedStatement prepStatement = conn.prepareStatement(SQL);
            if (args != null) {
                for (int i = 0; i < args.length; i++) {
                    prepStatement.setString(i + 1, args[i]);
                }
            }
            ResultSet rs = prepStatement.executeQuery();
            ResultSetMetaData meta = rs.getMetaData();
            int count = meta.getColumnCount();
            List<Map<String, String>> list = new ArrayList<Map<String, String>>();
            while (rs.next()) {
                Map<String, String> map = new HashMap<String, String>();
                for (int i = 1; i <= count; i++) {
                    map.put(meta.getColumnName(i), rs.getString(i));
                }
                list.add(map);
            }
            rs.close();
            prepStatement.close();
            conn.close();
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new PersistenceException();
        }
    }

    @Override
    public void update(String query) throws PersistenceException {
        try {
            Connection connection = this.cpds.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new PersistenceException();
        }
    }

    /**
     * 
     */
    @Override
    public void close() {
        this.cpds.close();
    }
}
