package com.jainil.gwt.sample.domain.login;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.jainil.gwt.sample.domain.dto.User;

@RemoteServiceRelativePath("login.rpc")
public interface LoginService extends RemoteService {

    /**
     * @param username
     * @param password
     * @return
     * @throws AuthServiceException
     */
    User getLogin(String username, String password) throws AuthServiceException;

    /**
     * @param username
     * @param password
     * @return
     * @throws AuthServiceException
     */
    User signUp(String username, String password) throws AuthServiceException;
}
