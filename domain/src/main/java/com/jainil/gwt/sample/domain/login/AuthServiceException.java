package com.jainil.gwt.sample.domain.login;

import com.google.gwt.user.client.rpc.IsSerializable;

public class AuthServiceException extends Exception implements IsSerializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1564797975195422839L;

    /**
     * Construct exception with default "Unknown Error" message
     */
    public AuthServiceException() {
        super("Unknown Error");
    }

    /**
     * @param message
     *            Construct exception with this message
     */
    public AuthServiceException(String message) {
        super(message);
    }

}
