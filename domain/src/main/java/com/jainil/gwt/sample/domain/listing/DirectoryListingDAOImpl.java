package com.jainil.gwt.sample.domain.listing;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.jainil.gwt.sample.domain.db.PersistenceAccess;
import com.jainil.gwt.sample.domain.db.PersistenceException;
import com.jainil.gwt.sample.domain.dto.FileMetaData;

public class DirectoryListingDAOImpl implements DirectoryLisitngDAO {

    private PersistenceAccess access;

    public void setAccess(PersistenceAccess aAccess) {
        this.access = aAccess;
    }

    @Override
    public List<FileMetaData> getList(Integer start, Integer end) throws PersistenceException {

        String queryString = "SELECT FileName, FilePath, FileVersion, FileDesc "
                + "FROM filedata where FileId BETWEEN ? AND ?";
        String[] args = { String.valueOf(start), String.valueOf(end) };

        List<FileMetaData> metaDataList = new ArrayList<FileMetaData>();
        List<Map<String, String>> results = this.access.executeQuery(queryString, args);
        for (Map<String, String> map : results) {
            FileMetaData file = new FileMetaData();
            file.setAll(map.get("FileName"), map.get("FilePath"), map.get("FileVersion"),
                    map.get("FileDesc"));
            metaDataList.add(file);
        }
        return metaDataList;
    }

    @SuppressWarnings("boxing")
    @Override
    public Integer getListSize() throws PersistenceException {
        String queryString = "SELECT max(FileId) FROM filedata";
        List<Map<String, String>> results = this.access.executeQuery(queryString, null);
        return Integer.getInteger(results.get(0).get("max(FileId"));
    }
}
