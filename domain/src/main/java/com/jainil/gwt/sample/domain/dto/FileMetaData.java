package com.jainil.gwt.sample.domain.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author jainil.parekh
 * 
 */
/**
 * @author jainil.parekh
 * 
 */
public class FileMetaData implements Serializable {

    private String fileName;
    private String fileVersion;
    private String fileDesc;
    private String filePath;

    /**
     * 
     */
    public FileMetaData() {
    }

    /**
     * @param aFileName
     * @param aFileVersion
     * @param aFileDesc
     * @param aFilePath
     */
    public void setAll(String aFileName, String aFileVersion, String aFileDesc, String aFilePath) {
        this.fileName = aFileName;
        this.fileVersion = aFileVersion;
        this.fileDesc = aFileDesc;
        this.filePath = aFilePath;
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return this.fileName;
    }

    /**
     * @param aFileName
     *            the fileName to set
     */
    public void setFileName(String aFileName) {
        this.fileName = aFileName;
    }

    /**
     * @return the fileVersion
     */
    public String getFileVersion() {
        return this.fileVersion;
    }

    /**
     * @param aFileVersion
     *            the fileVersion to set
     */
    public void setFileVersion(String aFileVersion) {
        this.fileVersion = aFileVersion;
    }

    /**
     * @return the fileDesc
     */
    public String getFileDesc() {
        return this.fileDesc;
    }

    /**
     * @param aFileDesc
     *            the fileDesc to set
     */
    public void setFileDesc(String aFileDesc) {
        this.fileDesc = aFileDesc;
    }

    /**
     * @return the filePath
     */
    public String getFilePath() {
        return this.filePath;
    }

    /**
     * @param aFilePath
     *            the filePath to set
     */
    public void setFilePath(String aFilePath) {
        this.filePath = aFilePath;
    }

    /**
     * Returns data map representation of the object
     */
    public Map<String, String> getDataMap() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("filename", this.fileName);
        map.put("path", this.filePath);
        map.put("version", this.fileVersion);
        map.put("description", this.fileDesc);
        return map;
    }
}
