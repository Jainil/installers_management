package com.jainil.gwt.sample.domain.listing;

import java.util.List;

import com.jainil.gwt.sample.domain.db.PersistenceException;
import com.jainil.gwt.sample.domain.dto.FileMetaData;

public interface DirectoryLisitngDAO {

    /**
     * @return total number of values in database
     */
    Integer getListSize() throws PersistenceException;

    /**
     * @param aStart
     * @param aEnd
     * @return
     * @throws PersistenceException
     */
    List<FileMetaData> getList(Integer aStart, Integer aEnd) throws PersistenceException;
}
