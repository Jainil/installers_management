package com.jainil.gwt.sample.domain.login;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Required;

import com.jainil.gwt.sample.domain.db.PersistenceAccess;
import com.jainil.gwt.sample.domain.db.PersistenceException;
import com.jainil.gwt.sample.domain.dto.User;

public class LoginServiceDAO {

    private PersistenceAccess access;

    // Logger logger = Logger.getLogger("ServletLoginLogger");

    /**
     * Default Constructor
     */
    public LoginServiceDAO() {
    }

    @Required
    public void setAccess(PersistenceAccess aAccess) {
        this.access = aAccess;
    }

    /**
     * @param username
     * @param password
     * @return
     * @throws PersistenceException
     * @throws AuthServiceException
     */
    User fetch(String username, String password) throws PersistenceException, AuthServiceException {

        String query = "Select * from users where username= ? ";
        String[] args = { username };
        List<Map<String, String>> result = this.access.executeQuery(query, args);

        if (result.isEmpty()) {
            throw new AuthServiceException("No records");
        }
        String usernameDB = result.get(0).get("username");
        String passwordDB = result.get(0).get("password");

        if (username.equals(usernameDB) && password.equals(passwordDB)) {
            // logger.log(Level.INFO, "Successfully retrieved login for: " +
            // username);
            return new User(username);
        }
        throw new AuthServiceException("Incorrect username and password");
    }

    /**
     * @param username
     * @param password
     * @throws PersistenceException
     */
    public void put(String username, String password) throws PersistenceException {

        String query = String.format("insert into users values ( '%s', '%s')", username, password);
        this.access.update(query);
    }
}
